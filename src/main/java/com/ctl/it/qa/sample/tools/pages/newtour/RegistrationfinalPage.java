package com.ctl.it.qa.sample.tools.pages.newtour;

import com.ctl.it.qa.staf.Page;
import com.ctl.it.qa.staf.xml.reader.IntDataContainer;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

public class RegistrationfinalPage extends Page {

	@FindBy(xpath = "//a[contains(text(),'REGISTER')]")
	public WebElementFacade btn_register_icon;

	@FindBy(xpath = "//input[@name=\"firstName\"]")
	public WebElementFacade tbx_first_name;

	@FindBy(xpath = "//input[@name=\"lastName\"]")
	public WebElementFacade tbx_last_name;

	@FindBy(xpath = "//input[@name=\"phone\"]")
	public WebElementFacade tbx_phone;

	@FindBy(xpath = "//input[@name=\"address1\"]")
	public WebElementFacade tbx_address;

	@FindBy(xpath = "//input[@name=\"city\"]")
	public WebElementFacade tbx_city;

	@FindBy(xpath = "//input[@name=\"state\"]")
	public WebElementFacade tbx_state;

	@FindBy(xpath = "//input[@name=\"email\"]")
	public WebElementFacade tbx_username;

	@FindBy(xpath = "//input[@name=\"password\"]")
	public WebElementFacade tbx_password;

	@FindBy(xpath = "//input[@name=\"confirmPassword\"]")
	public WebElementFacade tbx_confirmPassword;

	@FindBy(xpath = "//input[@name=\"register\"]")
	public WebElementFacade btn_register_final;

	@FindBy(xpath = "//img[@src='/images/masts/mast_register.gif']")
	public WebElementFacade img_register;

	@Override
	public WebElementFacade getUniqueElementInPage() {
		// TODO Auto-generated method stub
		return img_register;
	}

	public void testing_xmp_file()
	{
	IntDataContainer dataContainer = envData.getContainer("CommonData").getContainer(this.getClass().getSimpleName());
	String testvalue = dataContainer.getFieldValue("tbx_first_name") ;
	System.out.println(testvalue);
	
	}
	
}
