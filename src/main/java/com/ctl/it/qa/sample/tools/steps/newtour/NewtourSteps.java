package com.ctl.it.qa.sample.tools.steps.newtour;

import java.util.List;
import com.ctl.it.qa.sample.tools.pages.newtour.RegistrationfinalPage;
import com.ctl.it.qa.staf.Steps;
import com.ctl.it.qa.staf.xml.reader.IntContainerField;
import com.ctl.it.qa.staf.xml.reader.IntDataContainer;

import net.thucydides.core.annotations.Step;

@SuppressWarnings("serialbb")
public class NewtourSteps extends Steps {

	RegistrationfinalPage registrationfinalPage;

	String url = envData.getFieldValue("url");
	IntDataContainer dataContainer = envData.getContainer("CommonData").getContainer("RegistrationfinalPage");

	@Step
	public void login_newtour() {
		registrationfinalPage.openAt(url);
	}

	@Step
	public void newtour_registration_() {
		//registrationfinalPage.btn_register_icon.click();
		// registrationPage.tbx_firstname.sendKeys(dataContainer.getFieldValue("tbx_firstname"));a
		fillAllFields(registrationfinalPage, dataContainer);
		//registrationfinalPage.btn_register_final.click();
	}

	@Step
	public void success_validation() {
		System.out.println("Step - success_validation");
		shouldExist(registrationfinalPage, 30);
		registrationfinalPage.testing_xmp_file();
		
	}

}